const
  hyperlinker = require(`hyperlinker`),
  supportsHyperlinks = require(`supports-hyperlinks`),
  { getBorderCharacters, table } = require(`table`);

const displayHelpMessage = (flagHelp) => {
  const link = supportsHyperlinks.stdout ? hyperlinker(`https://www.gitlab.com/vaemoi/tooly/trf`) : ``;
  const flags = table(flagHelp, {
    border: getBorderCharacters(`void`),
    columnDefault: {
      paddingLeft: 1,
      paddingRight: 1
    },
    columns: {
      1: {
        paddingLeft: 10
      }
    },
    drawHorizontalLine: () => { return false; }
  });

  return `
USAGE
  $ trf [FLAGS] STRING1 [STRING2]

FLAGS
  ${flags.trim()}

ARGUMENTS
  Input is taken from stdin

EXTRA
The flags are processed in this order:
(1) -C, The complement if present is determined first

(2) -d, The characters or complement of characters from STRING1 are deleted second

(3) -s, The removal of multiple occurences of characters is done after complements and or deletions

=========================
Read more here: ${link}`;
};

module.exports = {
  show: displayHelpMessage,
};
