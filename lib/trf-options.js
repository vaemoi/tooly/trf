const { flags: Flags } = require(`cli-flags`);

module.exports = {
  flags: {
    complement: Flags.boolean({
      char: `C`,
      description: `Use the complement of the set of characters in in STRING1`
    }),
    excise: Flags.boolean({
      char: `d`,
      description: `Delete the set of characters from stdin that are present in STRING1`
    }),
    help: Flags.boolean({
      char: `h`,
      description: `Show flags their descriptions and help text for trf`
    }),
    squeeze: Flags.boolean({
      char: `s`,
      description: `Excise repeated sets of characters from the last operand (STRING1 or STRING2).`
    }),
    version: Flags.boolean({
      char: `v`,
      description: `Show the current version of trf installed`
    })
  }
};
