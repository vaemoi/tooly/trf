#!/usr/bin/env node

const { flagUsages: OptHelp, parse } = require(`cli-flags`);

const
  trf = require(`../lib/trf.js`),
  trfHelp = require(`../lib/trf-help.js`),
  trfOpts = require(`../lib/trf-options.js`);

const { flags } = parse(trfOpts);

if (flags.version) {
  console.log(`trf version: ${require(`../package.json`).version}`);
} else if (flags.help) {
  console.log(
    trfHelp.show(
      OptHelp(Object.values(trfOpts.flags), {displayRequired: true})
    )
  );
} else {
  trf(flags.complement, flags.excise, flags.squeeze);
}

